# Author: Stephan Frickenhaus

read.csv("repository_scoring_F_Thiesen_2023-04-08.csv")->r
summary(r)
head(r)
hist(r$F1..persistentID)
names(r)
r$X<-NULL
hist(r$reusable.score)
require(MASS)

# zero total scores
which(r$total.score==0)

r.scores=r[,c(5,9,11,13)] # rownames kept
names(r.scores)<-c("F","A","I","R") # colnames shorter
princomp(r.scores)-> pca.1

biplot(pca.1,cex=0.45)

#plot(r.scores)
w=c(43,163,77,141,38,81,112,44,49) # after 49 from locator
lab=rep("x",nrow(r.scores))
r$title[w]
r$title[w[1]] <- "DKRZ-WDCC"
r$title[w[8]] <- "IODP"
r$title[w[6]] <- "Europ.Gesch.online"

r$title[w]


lab[w]<-r$title[w] # w from original rownames
#plot(pca.1$scores[,1:2],col=1,pch=".")
#text(pca.1$scores[w,1:2],labels=r$title[w],cex=0.5)
lab[which(r$total.score==0)]="o"
# Fig. Poster
biplot(pca.1,xlabs=lab,cex=1.15,xlim=c(-0.15,0.2),expand=0.9)

sco=rep("",nrow(r))
sco[w]=as.character(round(r$total.score[w]*4,1))

biplot(pca.1,xlabs=paste(sco,lab),cex=1.15,xlim=c(-0.15,0.2),expand=0.9)


# locate few points
#p=locator(1)
#which.min(abs(p$x-pca.1$scores[,1])+abs(p$y-pca.1$scores[,2]))
# 29, 43, 3, 141
w1=c(29, 43, 3, 141)
lab1<-lab
lab1[w1]<-r$title[w1] # w from original rownames
lab1[29]<-"WDC-RSAT"
sco[w1]<-as.character(round(r$total.score[w1]*4,1))
biplot(pca.1,xlabs=paste(sco,lab1,sep=":"),cex=1.15,xlim=c(-0.12,0.175),expand=0.75)
legend("topleft",pch=NA,legend=c("F=finadable score","A=accessible score","I=interoperable score","R=reusable score","Label format:"," sum(scores) Name"),
       text.col=c(2,2,2,2,1,1))


table(r.scores$F)
table(r.scores$A)
table(r.scores$I)
table(r.scores$R)


######## END OF CODE ##########



sco=rep("x",nrow(r))
sco[w]=as.character(round(r$total.score[w],2))
biplot(pca.1,xlabs=sco,cex=0.45)
biplot(pca.1,xlabs=paste(sco,lab),cex=0.65)

plot(pca.1$scores,pch=".")

# 
d<-dist(r[,-c(1,2,14)])
plot(d[])
cmdscale(d,add=TRUE)->cmd
plot(cmd$points,pch=".",cex=0.5)
text(cmd$points,labels = r$title)

princomp(r[,-2:-1])->pca
par(mar=c(2,2,2,2))
biplot(pca,cex=0.5,xlab="",ylab="")


require(kohonen)
som(as.matrix(r.scores[w,]))->s
plot(s)

par(mfrow=c(1,4),mar=c(4,4,2,2))
boxplot(r$findable.score,main="F")
boxplot(r$accessible.score,main="A")
boxplot(r$interoperable.score,main="I")
text(0.85,0.5,label=paste("N=",sum(r$reusable.score!=0)))

boxplot(r$reusable.score,main="R")
text(0.85,0.5,label=paste("N=",sum(r$reusable.score!=0)))
# preliminary results from our score definition
#   - idea, approach/definitaion, current status of scoring
#          exploration of results
#

#
# goals: discuss approach, and how a well balanced FAIR-U score should be displayed
#    radial-plot as example for the principle, not for the design
#   
#
