# Copyright 2023 SGN
#
# SPDX-License-Identifier: CC0-1.0

"""Setup script for the nfdi4earth-label package."""
import versioneer
from setuptools import setup

setup(
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
)
