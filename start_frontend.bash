#!/bin/bash
#
# Starting the frontendd requires npm. We suggest to install it via nvm, see the README in this repo for details.
# The npm version in the Linux distro repos is often very old.
source bin/activate
cd ../nfdi4earth-label-frontend/
npm start
