# Low-level cordra functions.

import logging
import json
import requests
import dotenv
import os
from typing import Union, List, Tuple, Dict
import urllib


from .settings import get_settings
from .common import (
    get_eval_default,
    AssessmentPart,
    get_latest_eval_instance,
    get_datetime_now_in_cordra_format,
    get_repo_api_url,
    get_official_eval_instance,
    generate_uuid
)
from .webapi import WebApi
from .subasm_scoring import compute_subassessmentscore_for_selfassessment, compute_subassessmentscore_for_fuji, compute_subassessmentscore_for_re3data, get_re3data_score_required_to_get_label, get_fuji_score_required_to_get_label, get_selfassessment_score_required_to_get_label, get_max_score_possible_for_re3data, get_max_score_possible_for_fuji, get_max_score_possible_for_selfassessment, compute_subasm_passed_re3data, compute_subasm_passed_fuji, compute_subasm_passed_selfassessment

logger = logging.getLogger(__name__)

settings = get_settings()
VARIABLES_ENV_FILE = settings.get("VARIABLES_ENV_FILE")
IS_PRODUCTION = settings.get("IS_PRODUCTION")


def get_cordra_cfg(env_file: str = VARIABLES_ENV_FILE) -> dict:
    """
    Reads the Cordra configuration from the given .env file and returns it as a dict.
    This should be the file `variables.env`, from the root of the `knowledge-hub-backend-setup` sibling repository.
    """
    if not os.path.isfile(env_file):
        raise Exception(f"Environment file '{env_file}' not found at '{os.path.abspath(env_file)}'.")
    cordra_cfg = dotenv.dotenv_values(env_file)
    cordra_cfg["CORDRA_USER"] = "admin"
    cordra_cfg["TIMEOUT"] = 10
    return cordra_cfg


def ensure_repo_assessment_in_cordra(
    repo_object_id: str, repo_name=None, cordra_cfg=get_cordra_cfg()
) -> Tuple[bool, dict]:
    """
    Ensure empty assessment datastructure for repo exists in cordra. If none is found, one is created.
    @param repo_object_id: the cordra ID or handle of the repo
    @param repo_name: only required if no assessment is found and a new one must be created, as the assessment contains a repo name. If not givemn this function will try to determine the name from the cordra database and take the first one in case several names are listed.
    @return whether the assessment datastructure already existed.
    """

    if repo_object_id is None:
        raise ValueError("ensure_repo_assessment_in_cordra: parameter 'repo_object_id' must not be None.")

    query = f"type:RepositoryEvalForLabel"
    resp_repo_assessments: list = query_cordra_objects(query, cordra_cfg=cordra_cfg)
    assessments_repo = [a for a in resp_repo_assessments if a["content"]["n4e:repository"]["handle"] == repo_object_id]

    if len(assessments_repo) > 0:
        if len(assessments_repo) == 1:
            logger.debug(f"Found {len(assessments_repo)} assessment structure for repo {repo_object_id} in database.")
            return True, assessments_repo[0]
        if len(assessments_repo) > 1:
            raise ValueError(
                f"Found {len(assessments_repo)} assessments structures for repo {repo_object_id} in database, expected at most 1. Please fix database contents."
            )
            # If you want to clean all Assessments from the database, the easiest way is to use a script from the knowledgehub-backend-setup
            # repository. It can be found in devops/cordra/delete_all_data.py, but be sure to run with the '--types' argument so it does not
            # delete ALL objects!

    else:
        logger.debug(
            f"Found {len(assessments_repo)} assessment structures for repo {repo_object_id} in database. Creating new one."
        )
        new_assessment = get_eval_default()
        if repo_name is None:
            repo_names = get_repo_names_for_id(repo_object_id, cordra_cfg=cordra_cfg)
            repo_name = repo_names[0] if len(repo_names) > 0 else "Unknown Repo"
        new_assessment["n4e:repository"]["handle"] = repo_object_id
        new_assessment["n4e:repository"]["name"] = repo_name

        post_cordra_object(new_assessment, "RepositoryEvalForLabel", cordra_cfg=cordra_cfg)

        resp_repo_assessments: list = query_cordra_objects(query, cordra_cfg=cordra_cfg)
        assessments_repo = [
            a for a in resp_repo_assessments if a["content"]["n4e:repository"]["handle"] == repo_object_id
        ]
        if len(assessments_repo) == 1:
            return False, assessments_repo[0]
        else:
            raise ValueError(f"Failed to create RepositoryEvalForLabel for repository {repo_object_id} in cordra.")


def _parse_titles(repo_struct: dict) -> list[str]:
    """Parse titles from a dict/JSON Repository object (according to the N4E KH schema)."""
    title_field = repo_struct["title"]
    if isinstance(title_field, str):
        return [title_field]
    elif isinstance(title_field, dict):
        return title_field.get("@value", [])
    elif isinstance(title_field, list):
        if len(title_field) > 0:
            first_title = title_field[0]
            if isinstance(first_title, str):
                return title_field  # it's already a list of strings
            elif isinstance(first_title, dict):
                return [title.get("@value") for title in title_field]
    return []


def get_repo_names_for_id(repo_object_id: str, cordra_cfg=get_cordra_cfg()) -> list[str]:
    """Get the list of names for a repository ID from cordra."""
    if repo_object_id is None:
        raise ValueError(f"get_repo_names_for_id: parameter repo_object_id must not be None.")
    repo_resp = find_cordra_object_by_id(object_id=repo_object_id, cordra_cfg=cordra_cfg)
    return _parse_titles(repo_resp)


def get_unique_repo_name_for_id(repo_object_id: str, cordra_cfg=get_cordra_cfg()) -> list[str]:
    names = get_repo_names_for_id(repo_object_id, cordra_cfg)
    if len(names) == 1:
        return names[0]
    return None


def get_all_repos_from_cordra() -> dict:
    """Retrieve all repos from cordra
    @return the cordra response as JSON
    """
    return find_cordra_objects_by_type("Repository")


def get_all_assessments_from_cordra() -> dict:
    """Retrieve all assessments from cordra
    @return the cordra response as JSON
    """
    return find_cordra_objects_by_type("RepositoryEvalForLabel")


def get_repo_oai_pmh_api_url(repo_id: str):
    """
    Query cordra for eval result of the repo, then extract OAI-PMH API Url, if any. Extraction
    of the API URL requires that at least one sub assessment of type re3data is part of the eval result. If several exist, the relevant one will be used.
    @return the API URL as a string, or None if none could be found
    """
    logger.info(f"get_repo_oai_pmh_api_url: called for repo {repo_id}")
    eval_result = None
    response = get_all_assessments_from_cordra()
    for _, assessment in enumerate(response["results"]):
        if assessment["content"]["n4e:repository"]["handle"] == repo_id:
            eval_result = assessment["content"]

    if eval_result is None:
        print(f"get_repo_oai_pmh_api_url: found no RepositoryEvalForlabel for repo with ID '{repo_id}', cannot retrieve API URL.")
        return None

    if len(eval_result["results"]["re3data"]) >= 1:
        return get_repo_api_url(eval_result, WebApi.API_OAI_PMH)
    else:
        print(f"get_repo_oai_pmh_api_url: found no re3data eval result for repo with ID '{repo_id}', cannot retrieve API URL. Run re3data assessment first.")
        return None


def post_cordra_object(
    upload_object: dict,
    object_type: str,
    id: Union[str, None] = None,
    response_id_field: str = "@id",
    cordra_cfg: dict = get_cordra_cfg(),
) -> Union[None, str]:
    """
    POSTs a new object to Cordra.

    upload_object: the object to be uploaded. It will be converted to JSON by this function, so it should be a dict.
    object_type: the type of object to be uploaded
    id: the id to use for the object to be uploaded, optional. If not provided, a new id will be generated.
    response_id_field: the field in the response JSON that contains the id of the created object. Required to be able to return the ID of the created object, of creation succeeded. This is needed because Cordra uses different fields for different object types.
    """
    obj_json = json.dumps(upload_object)
    id_info_part = f" with id '{id}'" if id is not None else " without id"
    logger.debug(f"POSTing new object of type '{object_type}' {id_info_part} to cordra: {obj_json}")

    # create cordra object of proper type
    # use codra API to POST, consider updating if exists

    # https://www.cordra.org/documentation/api/rest-api.html
    create_object_url = cordra_cfg["CORDRA_BASE_URI"] + f"/objects?type={object_type}"

    if id is not None:
        create_object_url += f"&id={id}"

    response = requests.post(
        create_object_url,
        headers={"Content-type": "application/json"},
        data=obj_json,
        auth=requests.auth.HTTPBasicAuth(cordra_cfg["CORDRA_USER"], cordra_cfg["CORDRA_ADMIN_PASS"]),
        timeout=cordra_cfg["TIMEOUT"],
    )

    logger.debug(f"post_cordra_object: response code: {response.status_code}, text : {response.text}")

    if response.status_code == 409:
        logger.warning(
            f"POSTing {object_type} object failed: Object with id '{id}' already exists in Cordra. Continuing."
        )
        return None
    elif response.status_code != 200:
        logger.error(f"Error creating the new Cordra {object_type}: {response.text}")
        raise Exception(f"Error creating the new Cordra {object_type}: {response.text}")
    else:
        logger.debug(f"Created object of type '{object_type}'")
        # import pdb; pdb.set_trace()
        created_id = response.json().get(response_id_field)
        logger.debug(f"Created object of type '{object_type}' with ID '{created_id}'.")
        return created_id


from fastapi import HTTPException


def find_cordra_object_by_id(object_id: str, cordra_cfg: dict = get_cordra_cfg()) -> Union[None, Dict]:
    # https://www.cordra.org/documentation/api/rest-api.html
    # https://www.cordra.org/documentation/api/search.html

    if object_id is None:
        raise ValueError(f"find_cordra_object_by_id: parameter object_id must not be None.")

    find_object_url = cordra_cfg["CORDRA_BASE_URI"] + f"/objects/{object_id}"

    response = requests.get(
        find_object_url,
        headers={"Content-type": "application/json"},
        auth=requests.auth.HTTPBasicAuth(cordra_cfg["CORDRA_USER"], cordra_cfg["CORDRA_ADMIN_PASS"]),
    )

    if response.status_code == 200:
        return response.json()
    else:
        raise HTTPException(404, f"No Cordra object with ID '{object_id}' found")



def repo_exists_in_cordra(repo_id: str):
    return find_cordra_object_by_id(repo_id) is not None


def find_cordra_objects_by_type(object_type: str, cordra_cfg: dict = get_cordra_cfg()) -> Union[None, dict]:
    # https://www.cordra.org/documentation/api/rest-api.html
    # https://www.cordra.org/documentation/api/search.html
    find_object_url = cordra_cfg["CORDRA_BASE_URI"] + f"/objects/?query=type:{object_type}"

    response = requests.get(
        find_object_url,
        headers={"Content-type": "application/json"},
        auth=requests.auth.HTTPBasicAuth(cordra_cfg["CORDRA_USER"], cordra_cfg["CORDRA_ADMIN_PASS"]),
    )

    if response.status_code == 200:
        logger.debug(f"Received reply for objects of type '{object_type}'.")
        return response.json()
    else:
        logger.error(
            f"Did not find any object of type '{object_type}': HTTP code {response.status_code} : '{response.text}'"
        )
        response.raise_for_status()


def query_cordra_objects(query: str, cordra_cfg: dict = get_cordra_cfg()) -> Union[None, dict]:
    """
    Query cordra objects via the Cordra API.
    @note See https://www.cordra.org/documentation/api/rest-api.html and https://www.cordra.org/documentation/api/search.html
    @note Example query 1: "type:Repository AND n4e:sourceSystem:test"
    @note Example query 2: '/username:"bert"'
    """
    # logger.info(f"Finding cordra objects with query '{query}'.")

    logger.debug(f"querying existing cordra objects with cfg: {cordra_cfg}")
    response = requests.get(
        cordra_cfg["CORDRA_BASE_URI"] + "/search",
        auth=requests.auth.HTTPBasicAuth(cordra_cfg["CORDRA_USER"], cordra_cfg["CORDRA_ADMIN_PASS"]),
        params={"query": query},
        timeout=10,
    )
    if response.status_code == 200:
        num_found = len(response.json()["results"])
        # logger.info(f"Found {num_found} objects.")
        if num_found >= 1:
            return response.json()["results"]
        else:
            # logger.info(f"Did not find any object with query. Response code {response.status_code}: '{response.text}'")
            return []
    else:
        logger.error(f"Search query error for query '{query}'. Response code {response.status_code}: '{response.text}'")
        response.raise_for_status()


def update_cordra_object(update_object: dict, object_id: str, cordra_cfg: dict = get_cordra_cfg()) -> None:
    """
    Updates a Cordra object by its ID.
    """
    logger.debug(f"Updating cordra object with ID '{object_id}'.")
    obj_json = json.dumps(update_object)
    print(f"Updating object with ID '{object_id}' with JSON:\n{obj_json}")
    update_object_url = cordra_cfg["CORDRA_BASE_URI"] + f"/objects/{object_id}"
    response = requests.put(
        update_object_url,
        headers={"Content-type": "application/json"},
        data=obj_json,
        auth=requests.auth.HTTPBasicAuth(cordra_cfg["CORDRA_USER"], cordra_cfg["CORDRA_ADMIN_PASS"]),
    )
    if response.status_code == 200:
        logger.debug("Successfully updated cordra object with id '{object_id}'.")
    else:
        logger.error(
            f"Failed to update cordra object with ID '{object_id}'. HTTP status code: {response.status_code} : '{response.text}'"
        )
        response.raise_for_status()


def delete_cordra_object(object_id: str, cordra_cfg: dict = get_cordra_cfg()):
    """
    Deletes a Cordra object by its ID.
    """
    logger.debug(f"Deleting cordra object with ID '{object_id}'.")
    res_delete = requests.delete(
        cordra_cfg["CORDRA_BASE_URI"] + "/objects/" + object_id,
        headers={"Content-Type": "application/json"},
        auth=requests.auth.HTTPBasicAuth(cordra_cfg["CORDRA_USER"], cordra_cfg["CORDRA_ADMIN_PASS"]),
    )
    logger.debug(f"Deleted object with ID '{object_id}'.")
    return res_delete


def call_cordra_object_method(
    object_id: str, method: str, json_body: dict, attributes: dict = {}, cordra_cfg: dict = get_cordra_cfg()
) -> Union[None, dict]:
    """
    Call a cordra API method defined for an object. The method must be defined as a Javascript function in the
    knowledgehub-backend-setup repo, in the devops/cordra/ directory, and the schema needs to be updated in Cordra after
    changes.
    @note See https://www.cordra.org/documentation/api/rest-api.html
    """
    # https://www.cordra.org/documentation/api/rest-api.html
    if object_id is None:
        raise ValueError("call_cordra_object_method: parameter 'object_id' must not be None.")
    encoded_params = urllib.parse.urlencode({"objectId": object_id, "method": method})
    call_method_url = cordra_cfg["CORDRA_BASE_URI"] + f"/call?{encoded_params}"

    response = requests.post(
        call_method_url,
        headers={"Content-type": "application/json"},
        auth=requests.auth.HTTPBasicAuth(cordra_cfg["CORDRA_USER"], cordra_cfg["CORDRA_ADMIN_PASS"]),
        json=json_body,
    )

    if response.status_code == 200:
        return response.json()
    else:
        response.raise_for_status()
        return None


def _check_relevant_parts_names_okay(parts: List[str]) -> Tuple[bool, Union[str, None], str]:
    """
    Validation function for relevant_parts in assessment.
    @param parts: list of user supplied strings, this function checks whether they are vales from the ```AssessmentPart``` enum.
    @return Tuple of the following: (1) bool, whether the elements are okay.
    (2) the first culprit from the list that was not okay, if the first return value is False, otherwise None.
    (3) A message describing the problem, or the empty string if everything was okay.
    """

    valid_parts = [a.value for a in AssessmentPart]
    for part in parts:
        if not part in valid_parts:
            return False, part, f"Part {part} not in valid parts {valid_parts}"
    return True, None, ""


def extract_repo_name(cordra_repo_dict : Dict) -> Union[str, None]:
    """
    Extract the repository name from a Cordra repository object.
    """
    title_field = cordra_repo_dict["content"]["title"]
    if isinstance(title_field, str):
        return title_field
    elif isinstance(title_field, dict):
        return title_field.get("@value", None)
    elif isinstance(title_field, list):
        if len(title_field) > 0:
            first_title = title_field[0]
            if isinstance(first_title, str):
                return first_title  # it's already a list of strings
            elif isinstance(first_title, dict):
                return first_title.get("@value", None)
    return None


def get_repo_id_for_name(repo_name: str, cordra_cfg: dict = get_cordra_cfg()) -> Union[str, None]:
    """
    Get the Cordra ID for a repository name.
    Queries repo information from cordra and parses the title field to detect the name.
    @return the Cordra ID for the repository, or None if no repository with that name was found.
    """
    response = find_cordra_objects_by_type("Repository", cordra_cfg=cordra_cfg)
    for repo in response.get("results", {}):
        current_repo_name = extract_repo_name(repo)
        if current_repo_name == repo_name:
            return repo["content"]["id"]
    return None


def upload_repo_eval_result(
    eval_object,
    relevant_parts: List[str],
    cordra_cfg: dict = get_cordra_cfg(),
    create_repo_if_not_exists=False,
) -> None:
    """
    Upload the evaluation results sub parts to the Cordra database. This function can cope with existing RepositoryEvalForLabel instances in cordra,
    it will only add the new parts listed in relevant_parts in that case.
    @param eval_object: JSON with information on evaluation results, format must follow the schema for RepositoryEvalForLabel (see JSON schema in knowledge-hub-backend-setup repo)
    @param relevant_parts : the result parts of the full structure that should be uploaded (keys from the eval_object["results"] dictionary). Entries should be string from enum common.py/AssessmentPart. E.g., if you only did re3data, only put that string.
    @param cordra_cfg: dict with CORDRA configuration, see variables.env in knowledge-hub-backend-setup repo
    @return:
    """

    if eval_object["n4e:repository"]["handle"] is None:
        raise ValueError(
            "upload_repo_eval_result: the repo id for the assessment (eval_object[n4e:repository][handle]) must not be None."
        )

    eval_json = json.dumps(eval_object)
    if len(relevant_parts) == 0:
        logger.debug(f"Skipping upload of evaluation result: no relevant parts.")

    is_okay, culprit, msg = _check_relevant_parts_names_okay(relevant_parts)
    if not is_okay:
        raise ValueError("upload_repo_eval_result: " + culprit + ": " + msg)

    valid_parts = ["re3data", "selfassessment", "fuji"]
    for part in relevant_parts:
        if not part in valid_parts:
            raise ValueError(f"Invalid part '{part}', must be one of: {valid_parts}")
        num_of_that_part = len(eval_object["results"][part])
        if num_of_that_part != 1:
            raise ValueError(
                f"Requested upload of assessment result part {part} from eval objects, but the list in the object has length {num_of_that_part}. Expected length exactly 1."
            )

    logger.debug(f"Putting result parts {relevant_parts} of evaluation: {eval_json}")

    if create_repo_if_not_exists and not IS_PRODUCTION:
        repo_name = eval_object["n4e:repository"]["name"]
        repo_id = eval_object["n4e:repository"]["handle"]
        obj = find_cordra_object_by_id(object_id=repo_id, cordra_cfg=cordra_cfg)
        if obj is None:
            logger.info(f"Creating test repo with handle '{repo_id}' because it does not exist.")
            local_repo_id = create_test_repo(cordra_cfg, repo_id, repo_title=repo_name)
            logger.info(f"Local repo id is '{local_repo_id}'.")
            eval_object["n4e:repository"]["handle"] = local_repo_id  # overwrite with local ID
            eval_json = json.dumps(eval_object)

    # check if the repo exists in cordra, if not, create it
    repo_id = eval_object["n4e:repository"]["handle"]

    _, current_assessment_struct_in_db = ensure_repo_assessment_in_cordra(repo_id, cordra_cfg=cordra_cfg)

    assessment_id = current_assessment_struct_in_db["id"]

    new_assessment = current_assessment_struct_in_db.copy()

    ## The backend method for updating: edit the RepositoryEvalForLabel instance in the Python code, then UPDATE it in Cordra with an update cordra API call.
    for part in relevant_parts:
        relevant_assessment = eval_object["results"][part][0] # we checked above that the lenfth is exactly 1.
        new_assessment["content"]["results"][part].append(relevant_assessment)

    # Important: fill in all the general fields in the new assessment, such as the achievedN4ELabel for latest and official, and the evaluated_at field.
    new_assessment["content"] = recompute_global_result(new_assessment["content"])

    ensure_subassessmentscores_complete(asm_content=new_assessment["content"], logtag="upload_repo_eval_result")

    update_cordra_object(new_assessment["content"], assessment_id, cordra_cfg=cordra_cfg)


valid_asmtypes = ["re3data", "fuji", "selfassessment"]


def get_official_eval_instances(asm_content: dict) -> dict:
    """
    Get the official sub assessment instances, i.e., those marked as official via their UUID, from the API result dict.
    @return a dict with keys 're3data', 'fuji', and 'selfassessment'. Note that there may not be any official assessments for one or more sub assessment types, in that case the respective value will be None.
    """

    re3data_assessment = None
    fuji_assessment = None
    selfassessment_assessment = None

    sub_asm_ids = asm_content["results"]["achievedN4ELabel"]["official"]["based_on_subassessments"]

    re3data_uuid = sub_asm_ids.get("re3data_subassessment_uuid", "")
    fuji_uuid = sub_asm_ids.get("fuji_subassessment_uuid", "")
    selfassessment_uuid = sub_asm_ids.get("selfassessment_subassessment_uuid", "")

    if len(re3data_uuid) > 0:
        re3data_assessment = get_official_eval_instance(asm_content["results"]["re3data"], uuid=re3data_uuid)

    if len(fuji_uuid) > 0:
        fuji_assessment = get_official_eval_instance(asm_content["results"]["fuji"], uuid=fuji_uuid)

    if len(selfassessment_uuid) > 0:
        selfassessment_assessment = get_official_eval_instance(asm_content["results"]["selfassessment"], uuid=selfassessment_uuid)

    return { "re3data" : re3data_assessment, "fuji" : fuji_assessment, "selfassessment" : selfassessment_assessment }


def ensure_subassessmentscores_complete(asm_content, logtag : str = ""):
    for subasm_type in ["re3data", "fuji", "selfassessment"]:
        print(f"TTT {logtag} Checking subassessments of type {subasm_type}, with {len(asm_content['results'][subasm_type])} entries.")
        for idx, subasm in enumerate(asm_content["results"][subasm_type]):
            for expected_entry in ["score_metrics_considered", "subassessment_passed"]:
                try:
                    if not expected_entry in subasm["subassessmentscore"]:
                        raise ValueError(f"TTT1 {logtag} Expected entry {expected_entry} not found in {subasm_type} asm {subasm}.")
                    else:
                        print(f"TTT2 {logtag} : KKK found expected entry {expected_entry} in {subasm_type} sub assessment #{idx}.")
                except Exception as ex:
                    print(f"TTT3 {logtag}: error checking {expected_entry} in {subasm_type} sub assessment #{idx} :{subasm} : {str(ex)}")
                    raise ex

def recompute_global_result(asm_content: dict) -> dict:
    """Re-compute the global Label metrics (scores for the 3 parts, whether the Label is achieved).
    This computes both the official and the latest results.
    @param asm_content JSON datastructure of the full assessment (a RepositoryEvalForLabel instance). If you got this from cordra, supply only the 'content' part.
    @return the modified asm_content, with the score fields under ```["results"]["achievedN4ELabel"]``` (both ```["results"]["achievedN4ELabel"]["official"]``` and ```["results"]["achievedN4ELabel"]["latest"]```) filled in.
    """

    latest_re3data_assessment = get_latest_eval_instance(asm_content["results"]["re3data"])
    latest_fuji_assessment = get_latest_eval_instance(asm_content["results"]["fuji"])
    latest_selfassessment_assessment = get_latest_eval_instance(asm_content["results"]["selfassessment"])

    latest_res = compute_achievedN4ELabel_for_assessments(latest_re3data_assessment, latest_fuji_assessment, latest_selfassessment_assessment, logtext="latest")

    official_subassessments = get_official_eval_instances(asm_content)
    official_res = compute_achievedN4ELabel_for_assessments(official_subassessments.get("re3data"), official_subassessments.get("fuji"), official_subassessments.get("selfassessment"), logtext="official")

    asm_content["results"]["achievedN4ELabel"]["latest"] = latest_res
    asm_content["results"]["achievedN4ELabel"]["official"] = official_res
    asm_content["results"]["evaluated_at"] = get_datetime_now_in_cordra_format()

    repo_id = asm_content['n4e:repository']['handle']
    repo_name = asm_content['n4e:repository']['name']

    ensure_subassessmentscores_complete(asm_content=asm_content, logtag="recompute_global_res")

    str_latest = f"re3data: {latest_res['score_re3data']}/{latest_res['score_re3data_max_possible']}, fuji: {latest_res['score_fuji']}/{latest_res['score_fuji_max_possible']}, selfassessment: {latest_res['score_selfassessment']}/{latest_res['score_selfassessment_max_possible']}, total: {latest_res['score_re3data'] + latest_res['score_fuji'] + latest_res['score_selfassessment']}"
    str_official = f"re3data: {official_res['score_re3data']}/{official_res['score_re3data_max_possible']}, fuji: {official_res['score_fuji']}/{official_res['score_fuji_max_possible']}, selfassessment: {official_res['score_selfassessment']}/{official_res['score_selfassessment_max_possible']}, total: {official_res['score_re3data'] + official_res['score_fuji'] + official_res['score_selfassessment']}"

    logger.debug(f"recompute_global_result: called for repo {repo_name} ({repo_id}), set the results for latest ({str_latest}) and official ({str_official}).")
    logger.debug(f"recompute_global_result: result is : {str(asm_content)}")

    return asm_content




def compute_achievedN4ELabel_for_assessments(re3data_assessment, fuji_assessment, selfassessment_assessment, logtext = None) -> Dict:

    log_tag = "" if logtext is None else f" ({logtext})"

    res = dict()
    res["based_on_subassessments"] = dict()
    res["based_on_subassessments"]["re3data_subassessment_uuid"] = ""
    res["based_on_subassessments"]["fuji_subassessment_uuid"] = ""
    res["based_on_subassessments"]["selfassessment_subassessment_uuid"] = ""
    res["suggestions"] = { "re3data" : [], "fuji" : [], "selfassessment" : [] }

    if re3data_assessment is not None:
        re3data_score = re3data_assessment["subassessmentscore"]["score_achieved"]
        re3data_score_max_possible = re3data_assessment["subassessmentscore"]["score_max_possible"]
        res["based_on_subassessments"]["re3data_subassessment_uuid"] = re3data_assessment["uuid"]
        # For all metrics that failed, add the suggestions on what to do for the repo representatives to the result. The suggestions are in each metric, in the field [<metricename>]["singlemetricscore"]["score_not_achieved_suggestion"].
        suggestions_re3data = []
        for metric in list(re3data_assessment["metrics"].keys()):
            if not re3data_assessment["metrics"][metric]["result"] == True:
                suggestions_re3data.append({ "metric" : metric, "suggestion" : re3data_assessment["metrics"][metric]["singlemetricscore"]["score_not_achieved_suggestion"]})
        res["suggestions"]["re3data"] = suggestions_re3data
    else:
        re3data_score = 0
        re3data_score_max_possible = get_max_score_possible_for_re3data() # Note: This is the max value for the CURRENT version, and the assessment may be older, but it does not matter much, as there IS NO sub assessment if this code runs.

    re3data_passed = compute_subasm_passed_re3data(re3data_assessment)

    logger.debug(f"re3data_score_max_possible={re3data_score_max_possible}")

    logger.debug(f"compute_achievedN4ELabel_for_assessments:{log_tag} re3data_passed: {re3data_passed}, with score {re3data_score} out of {re3data_score_max_possible}.")

    fuji_score = 0
    fuji_valid = False
    fuji_score_max_possible = get_max_score_possible_for_fuji()
    if fuji_assessment is not None:
        fuji_valid = fuji_assessment["scan"]["fuji_scan_technical_success"] == True
        res["based_on_subassessments"]["fuji_subassessment_uuid"] = fuji_assessment["uuid"]
        # Add all suggestions from fuji assessment to the result. The suggestions are in each metric, in the field [<metricename>]["singlemetricscore"]["score_not_achieved_suggestion"].
        suggestions_fuji = []
        for metric in list(fuji_assessment["metrics"].keys()):
            if not fuji_assessment["metrics"][metric]["result"] == True:

                suggestion = fuji_assessment["metrics"][metric]["singlemetricscore"]["score_not_achieved_suggestion"]
                print(f'fuji metric {metric} failed with suggestion: "{suggestion}" from singlemetricscore {str(fuji_assessment["metrics"][metric]["singlemetricscore"])}.')
                suggestions_fuji.append({ "metric" : metric, "suggestion" : suggestion})
        res["suggestions"]["fuji"] = suggestions_fuji

    if fuji_valid:
        fuji_score = fuji_assessment["subassessmentscore"]["score_achieved"]
        fuji_score_max_possible = fuji_assessment["subassessmentscore"]["score_max_possible"]


    fuji_passed = compute_subasm_passed_fuji(fuji_assessment)  # Currently we do not require a F-UJI assessment, as many repos have no API. So this is always True.

    logger.debug(f"compute_achievedN4ELabel_for_assessments:{log_tag} fuji_passed: {fuji_passed}, with score {fuji_score} out of {fuji_score_max_possible}.")

    selfassessment_score = 0
    selfassessment_score_max_possible = get_max_score_possible_for_selfassessment()
    if selfassessment_assessment is not None:
        selfassessment_score = selfassessment_assessment["subassessmentscore"]["score_achieved"]
        selfassessment_score_max_possible = selfassessment_assessment["subassessmentscore"]["score_max_possible"]
        res["based_on_subassessments"]["selfassessment_subassessment_uuid"] = selfassessment_assessment["uuid"]
        # Add all suggestions from selfassessment assessment to the result. The suggestions are in each metric, in the field [<metricename>]["singlemetricscore"]["score_not_achieved_suggestion"].
        suggestions_selfassessment = []
        for metric in list(selfassessment_assessment["metrics"].keys()):
            if not selfassessment_assessment["metrics"][metric]["result"] == True:
                suggestions_selfassessment.append({ "metric" : metric, "suggestion" : selfassessment_assessment["metrics"][metric]["singlemetricscore"]["score_not_achieved_suggestion"]})
        res["suggestions"]["selfassessment"] = suggestions_selfassessment

    selfassessment_passed = compute_subasm_passed_selfassessment(selfassessment_assessment)
    all_passed = re3data_passed and fuji_passed and selfassessment_passed

    logger.debug(f"compute_achievedN4ELabel_for_assessments:{log_tag} selfassessment_passed: {selfassessment_passed}, with score {selfassessment_score} out of {selfassessment_score_max_possible}.")
    logger.debug(f"""compute_achievedN4ELabel_for_assessments:{log_tag} re3data_passed: {re3data_passed}, fuji_passed: {fuji_passed}, selfassessment_passed: {selfassessment_passed}, all_passed: {all_passed}""")
    logger.debug(f"compute_achievedN4ELabel_for_assessments:{log_tag} based on subassessments: {res['based_on_subassessments']}")

    res["result"] = all_passed
    res["score_re3data"] = re3data_score
    res["score_re3data_max_possible"] = re3data_score_max_possible
    res["score_fuji"] = fuji_score
    res["score_fuji_max_possible"] = fuji_score_max_possible
    res["score_selfassessment"] = selfassessment_score
    res["score_selfassessment_max_possible"] = selfassessment_score_max_possible
    res["passed_re3data"] = re3data_passed
    res["passed_fuji"] = fuji_passed
    res["passed_selfassessment"] = selfassessment_passed
    res["evaluated_at"] = get_datetime_now_in_cordra_format()

    return res




def save_selfassessment_to_cordra(repo_id: str, acontent: dict):
    try:
        _ = acontent["metrics"]["hasBackupStrategy"]["result"]
    except Exception as _:
        raise ValueError("Invalid selfassessment part. Expected field 'metrics.hasBackupStrategy.result' not present.")
    acontent = compute_subassessmentscore_for_selfassessment(acontent)
    return save_assessment_part_to_cordra(repo_id, acontent, asmtype="selfassessment")


def save_re3data_assessment_to_cordra(repo_id: str, acontent: dict):
    try:
        _ = acontent["metrics"]["assignsPersistentIds"]["result"]
    except Exception as _:
        raise ValueError(
            "Invalid re3data assessment part. Expected field 'metrics.assignsPersistentIds.result' not present."
        )
    acontent = compute_subassessmentscore_for_re3data(acontent)
    return save_assessment_part_to_cordra(repo_id, acontent, asmtype="re3data")


def save_fuji_assessment_to_cordra(repo_id: str, acontent: dict):
    try:
        _ = acontent["metrics"]["hasDescriptiveMetadata"]["result"]
    except Exception as _:
        raise ValueError(
            "Invalid fuji assessment part. Expected field 'metrics.hasDescriptiveMetadata.result' not present."
        )
    acontent = compute_subassessmentscore_for_fuji(acontent)
    return save_assessment_part_to_cordra(repo_id, acontent, asmtype="fuji")


def save_assessment_part_to_cordra(repo_id: str, acontent: dict, asmtype: str):
    if asmtype not in valid_asmtypes:
        raise ValueError(f"ERROR: argument asmtype must be one of {valid_asmtypes}")

    # Add UUID for this new assessment.
    if acontent.get("uuid", None) is None:
        acontent["uuid"] = generate_uuid()

    full_eval_struct = get_eval_default(repo_name="PANGAEA")
    full_eval_struct["n4e:repository"]["handle"] = repo_id

    full_eval_struct["results"][asmtype].append(acontent) # The computation of the derived fields in the outer assessment struct (achievedN4ELabel.latest and official) is done in recompute_global_result, which is called in upload_repo_eval_result.

    existed, _ = ensure_repo_assessment_in_cordra(repo_id)
    if not existed:
        logger.info(f"Outer assessment struct did not exist for repo '{repo_id}' before query.")

    logger.info(f"Uploading evaluation result of type '{asmtype}' for repo with ID '{repo_id}'.")

    upload_repo_eval_result(full_eval_struct, relevant_parts=[asmtype])



def create_test_repo(cordra_cfg: dict, repo_id, repo_title: str) -> str:
    """
    Create test repository in Cordra. To do so, creates a user and an organization as well.
    @param cordra_cfg: config dict containing cordra API info
    @param repo_id: the id you want the new repo to have in cordra
    @param repo_title: the repo name
    """
    user_name: str = "testuser1"
    user_dict: dict = {
        "username": user_name,
        "password": "testuser1password",
        "requirePasswordChange": False,
    }

    u_id = post_cordra_object(user_dict, object_type="User", id="testuser1", cordra_cfg=cordra_cfg)
    if u_id is None:
        json_res = query_cordra_objects(query=f'/username:"{user_name}"', cordra_cfg=cordra_cfg)
        u_id = json_res[0]["id"]
        logger.debug(f"Using existing user ID: {u_id}")
    else:
        logger.debug(f"Created user ID: {u_id}")

    # Now create organization
    org_name = "Testorganization5"
    orga_dict = {
        "name": [org_name],
        "homepage": ["https://test5.org"],
        "sourceSystem": u_id,
    }

    logger.debug(f"Posting new organization for user.")
    org_id = post_cordra_object(orga_dict, object_type="Organization", id=org_name, cordra_cfg=cordra_cfg)
    if org_id is None:
        logger.debug(f"Received no organization ID from post.")
        # json_res = query_existing_cordra_objects(query=f'/foaf:name:"{org_name}"', cordra_cfg=cordra_cfg)
        json_res = query_cordra_objects(
            query=f'/name:"{org_name}"', cordra_cfg=cordra_cfg
        )  # TODO: this is currently broken.
        logger.debug(f"Queried cordra objects with name: {org_name}")
        if len(json_res) == 0:
            logger.error(f"Received no cordra objects with foaf:name '{org_name}'.")
        org_id = json_res[0]["id"]
        logger.debug(f"Using existing organization with ID: {org_id}")
    else:
        logger.debug(f"Created organization with ID: '{org_id}' (from POST).")
    logger.debug(f"Posted new organization for user.")

    # Then create repository owned by organization
    repo_dict = {
        "dct:publisher": [org_id],
        "dct:title": repo_title,
        "n4e:sourceSystem": u_id,
    }
    repo_id = post_cordra_object(repo_dict, object_type="Repository", id=repo_id, cordra_cfg=cordra_cfg)
    return repo_id
