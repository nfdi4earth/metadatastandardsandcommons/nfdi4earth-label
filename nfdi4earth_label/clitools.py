import argparse
import json
import sys

from .cordra_util import (
    find_cordra_objects_by_type,
    valid_asmtypes,
    save_assessment_part_to_cordra,
    get_unique_repo_name_for_id,
    save_fuji_assessment_to_cordra,
    save_re3data_assessment_to_cordra,
    get_repo_oai_pmh_api_url,
)
from .common import get_eval_default_selfassessment_entry, get_eval_default_re3data_entry, get_eval_default, get_eval_default_fuji_entry
from .fuji_data import compute_repo_fuji_eval_result
from .re3data import compute_repo_re3data_eval_result
import logging
from .webapi import WebApi
from .labelapi import set_subassessment_category_cmdline
from .common import get_repo_api_url
from .labelmail import send_testemail

from .settings import check_label_app_status, get_settings

settings = get_settings()
LABEL_SMTP_SECRET_MAIL_USER = settings.get("LABEL_SMTP_SECRET_MAIL_USER")
LABEL_SMTP_SECRET_MAIL_PASSWORD = settings.get("LABEL_SMTP_SECRET_MAIL_PASSWORD")
ENABLE_SMTP = settings.get("ENABLE_SMTP")


logger = logging.getLogger(__name__)


def list_repos() -> None:
    """List all repos from Cordra. This is the main function of the 'lbl_listrepos' command line script."""
    parser = argparse.ArgumentParser(description="List repos in cordra database")
    parser.add_argument("--noheader", action="store_true", help="Do not print header")
    parser.add_argument(
        "--raw", action="store_true", help="Output full raw JSON output instead of parsed fields in CSV format."
    )
    parser.add_argument("--api", action="store_true", help="Retrieve the OAI-PMH API URL from re3data, if any, and print it as well. This is ignored if --raw is set.")
    args = parser.parse_args()

    if args.raw and args.api:
        print("WARNING: --raw and --api are incompatible. Showing raw data without API information.")

    response = find_cordra_objects_by_type("Repository")
    if not args.noheader:
        if(args.api):
            print(f"Repo#, RepoID, RepoName, API URL")
        else:
            print(f"Repo#, RepoID, RepoName")

    for repo_idx, repo in enumerate(response["results"]):
        repo_id = repo['id']
        repo_name = _get_title(repo)
        if args.raw:
            print(f"Repo # {repo_idx + 1} of {len(response['results'])}:")
            print(f"{repo}")
        else:
            if(args.api):
                eval_result = compute_repo_re3data_eval_result(repo_id, repo_name)
                repo_oaipmh_api_url = get_repo_api_url(eval_result, WebApi.API_OAI_PMH)
                repo_oaipmh_api_url_text = f"API={repo_oaipmh_api_url}" if repo_oaipmh_api_url is not None else "N/A"
                print(f"{repo_idx}, {repo_id}, {repo_name}, {repo_oaipmh_api_url_text}")
            else:
                print(f"{repo_idx}, {repo_id}, {repo_name}")


def _get_title(repo_json):
    """Extract title from repo JSON."""
    title_arr = repo_json["content"]["title"]
    if type(title_arr) == str:
        return title_arr
    else:
        try:
            return title_arr[0]["@value"]
        except Exception as _:
            pass
    return None


def list_assessments() -> None:
    """List all assessments from Cordra. This is the main function of the 'lbl_listassessments' command line script."""
    parser = argparse.ArgumentParser(description="List Label assessments in cordra database")
    parser.add_argument("--noheader", action="store_true", help="Do not print header")
    parser.add_argument(
        "--raw", action="store_true", help="Output full raw JSON output instead of parsed fields in CSV format."
    )
    args = parser.parse_args()

    response = find_cordra_objects_by_type("RepositoryEvalForLabel")
    if not args.noheader:
        print(f"Assessment#, AsmID, RepoID, #r3d, #fuji, #saf, IsComplete, Result Official, Result Latest, EvalDate, RepoName")

    for assessment_idx, assessment in enumerate(response["results"]):
        if args.raw:
            print(f"Assessment # {assessment_idx + 1} of {len(response['results'])}:")
            print(f"{assessment}")
        else:
            crepo = assessment["content"]["n4e:repository"]
            cresults = assessment["content"]["results"]
            result_official = cresults['achievedN4ELabel']['official']['result']
            result_latest = cresults['achievedN4ELabel']['latest']['result']
            num_asm_re3data = len(assessment["content"]["results"]["re3data"])
            num_asm_fuji = len(assessment["content"]["results"]["fuji"])
            num_asm_selfassessment = len(assessment["content"]["results"]["selfassessment"])
            is_complete = num_asm_re3data >= 1 and num_asm_selfassessment >= 1
            print(
                f"{assessment_idx}, {assessment['id']}, {crepo['handle']}, {num_asm_re3data}, {num_asm_fuji}, {num_asm_selfassessment}, {is_complete}, {result_official}, {result_latest}, {cresults['evaluated_at']}, {crepo['name']}"
            )


def print_sub_assessments(sub_assessments_array : any, info_dict : dict, atype : str):
    """Util funciton used in list_subassessments to print all sub assessments of a type (re3data, fuji, selfassessment)"""
    raw = info_dict["raw"]
    assessment_id = info_dict["assessment_id"]
    repo_id = info_dict["repo_id"]
    repo_name = info_dict["repo_name"]
    for subassessment_group_idx, subassessment in enumerate(sub_assessments_array):
        if raw:
            print(f"{atype} sub Assessment # {subassessment_group_idx + 1} of {len(sub_assessments_array)} for assessment {assessment_id} of repo {repo_id}:")
            print(f"{subassessment}")
        else:
            subasm_uuid = subassessment["uuid"]
            subasm_num_of_group = f"{subassessment_group_idx + 1}/{len(sub_assessments_array)}"
            print(
                f"{assessment_id}, {repo_id}, {atype}, {subasm_uuid}, {subasm_num_of_group}, {repo_name}"
            )


def list_subassessments() -> None:
    """List all subassessments for an assessment from Cordra. This is the main function of the 'lbl_listsubassessments' command line script."""
    parser = argparse.ArgumentParser(description="List Label sub assessments of an assessment in cordra database")
    parser.add_argument('--assessmentid', type=str, default="ALL",
                    help='The assessment ID for which to list the sub assessments. Get it by running the \'lbl_listassessments\' app or supply \'ALL\' for all assessments.')
    parser.add_argument("--noheader", action="store_true", help="Do not print header")
    parser.add_argument(
        "--raw", action="store_true", help="Output full raw JSON output instead of parsed fields in CSV format."
    )
    parser.add_argument(
        "--skip-re3data", action="store_true", help="Skip re3data sub assessments in output."
    )
    parser.add_argument(
        "--skip-fuji", action="store_true", help="Skip fuji sub assessments in output."
    )
    parser.add_argument(
        "--skip-selfassessment", action="store_true", help="Skip selfassessment sub assessments in output."
    )
    parser.add_argument(
        "--only-official", action="store_true", help="Show only official sub assessments in output."
    )
    parser.add_argument(
        "--only-latest", action="store_true", help="Show only latest sub assessments in output."
    )
    args = parser.parse_args()

    if args.skip_re3data and args.skip_fuji and args.skip_selfassessment:
        print("Nothing to do, all 3 assessment types skipped. Exiting.")
        sys.exit(0)

    if args.only_official and args.only_latest:
        print("Only one of '--only-official' and '--only-latest' can be specified.")
        sys.exit(1)

    response = find_cordra_objects_by_type("RepositoryEvalForLabel")
    if not args.noheader:
        print(f"FullAsmID, RepoID, SubAsmType, SubAsmUUID, SubAsmNumber, RepoName")

    relevant_results = response["results"]
    if len(relevant_results) == 0:
        print(f"No assessments found in database, exiting.")
        sys.exit(0)

    asmid = args.assessmentid
    if asmid not in ["ALL", "all"]:
        relevant_results = [r for r in relevant_results if r['id'] == asmid]
        if len(relevant_results) == 0:
            print(f"No assessment with ID '{asmid}' found in database. Run the lbl_listassessments app to find valid assessment IDs.")
    else:
        if len(relevant_results) == 0:
            print(f"No assessments found in database.")

    num_asm_total = len(relevant_results)
    num_skipped_no_subasm = 0
    for assessment_idx, assessment in enumerate(relevant_results):
        info_dict = {}
        info_dict["assessment_id"] = assessment['id']
        info_dict["repo_id"] = assessment["content"]["n4e:repository"]["handle"]
        info_dict["repo_name"] = assessment["content"]["n4e:repository"]["name"]
        info_dict["raw"] = args.raw

        re3data_subassessments = assessment["content"]["results"]["re3data"]
        fuji_subassessments = assessment["content"]["results"]["fuji"]
        selfassessment_subassessments = assessment["content"]["results"]["fuji"]

        if len(re3data_subassessments) == 0 and len(fuji_subassessments) == 0 and len(selfassessment_subassessments) == 0:
            num_skipped_no_subasm += 1

        if args.only_official or args.only_latest:
            if args.only_official:
                relevant_ids = assessment["content"]["results"]["achievedN4ELabel"]["official"]
            else:
                relevant_ids = assessment["content"]["results"]["achievedN4ELabel"]["latest"]
            relevant_re3data_id = relevant_ids["based_on_subassessments"].get("re3data_subassessment_uuid", "")
            relevant_fuji_id = relevant_ids["based_on_subassessments"].get("fuji_subassessment_uuid", "")
            relevant_selfassessment_id = relevant_ids["based_on_subassessments"].get("selfassessment_subassessment_uuid", "")
            if len(relevant_re3data_id) > 0:
                re3data_subassessments = [a for a in re3data_subassessments if a['id'] == relevant_re3data_id]
            if len(relevant_fuji_id) > 0:
                fuji_subassessments = [a for a in fuji_subassessments if a['id'] == relevant_fuji_id]
            if len(relevant_selfassessment_id) > 0:
                selfassessment_subassessments = [a for a in selfassessment_subassessments if a['id'] == relevant_selfassessment_id]


        if not args.skip_re3data:
            print_sub_assessments(re3data_subassessments, info_dict, "re3data")
        if not args.skip_fuji:
            print_sub_assessments(fuji_subassessments, info_dict, "fuji")
        if not args.skip_selfassessment:
            print_sub_assessments(selfassessment_subassessments, info_dict, "selfassessment")

    if num_skipped_no_subasm > 0:
        print(f"Note: Skipped {num_skipped_no_subasm} of {num_asm_total} assessments which did not have any sub assessments.")


def get_assessment_template():
    """Print assessment templates as JSON strings to command line."""
    parser = argparse.ArgumentParser(description="Get assessment template")
    parser.add_argument(
        "asmtype", type=str, help="Assessment type, one of 'outer', 're3data', 'fuji', or 'selfassessment'."
    )
    parser.add_argument('--reponame', type=str, default="",
                    help='Optional string. The repo name, only used for asmtype "outer". Run lbl_listrepos to find valid ones.')
    parser.add_argument('--repohandle', type=str, default="",
                    help='Optional string. The repo cordra ID or handle, only used for asmtype "outer". Run lbl_listrepos to find valid ones.')
    parser.add_argument('--positive', action="store_true", help="Whether the sub assessment's `result' field should be positive. Defaults to negative if omitted. Only usd for asmtype 're3data', 'fuji' and 'selfassessment'.")

    args = parser.parse_args()

    if args.positive and args.asmtype == "outer":
        print("Argument '--positive' is invalid for asmtype 'outer'.")
        sys.exit(1)

    if args.reponame and args.asmtype != "outer":
        print("Argument '--reponame' is only valid for asmtype 'outer'.")
        sys.exit(1)

    if args.repohandle and args.asmtype != "outer":
        print("Argument '--repohandle' is only valid for asmtype 'outer'.")
        sys.exit(1)

    if args.asmtype == "outer":
        print(json.dumps(get_eval_default(repo_name=args.reponame, repo_handle=args.repoid), ensure_ascii=True, indent=4))
    elif args.asmtype == "re3data":
        print(json.dumps(get_eval_default_re3data_entry(result_value=args.positive), ensure_ascii=True, indent=4))
    elif args.asmtype == "fuji":
        print(json.dumps(get_eval_default_fuji_entry(result_value=args.positive), ensure_ascii=True, indent=4))
    elif args.asmtype == "selfassessment":
        print(json.dumps(get_eval_default_selfassessment_entry(result_value=args.positive), ensure_ascii=True, indent=4))
    else:
        print(f"Invalid argument 'asmtype': must be one of: 'outer', 're3data', 'fuji', or 'selfassessment'.")
        sys.exit(1)
    sys.exit(0)

def set_subassessment_category():
    """
    Main function for the lbl_setsubasmcategory app.
    Allows setting the category of a sub assessment in the Cordra database.
    """
    parser = argparse.ArgumentParser(description="Set sub assessment category")

    parser.add_argument("repoid", type=str, help="The internal Cordra identifier of the repo to which the sub assessment belongs. Find the repo ID for a repo name by running the 'lbl_listrepos' app. Find all information on a sub assessment by running the 'lbl_listsubassessments' app. Shoud look like 'n4d/r3d-xxxxxxxxx'.")
    parser.add_argument("assessmentid", type=str, help="The internal Cordra identifier of the outer assessment to which the sub assessment belongs. Find the repo ID for a repo name by running the 'lbl_listrepos' app. Find all information on a sub assessment by running the 'lbl_listsubassessments' app. Shoud look like 'n4dxxxxxxxxxxxx'.")
    parser.add_argument("subassessmentuuid", type=str, help="The sub assessment UUID (unique identifier). Find all information on a sub assessment by running the 'lbl_listsubassessments' app. An example is 54b858ca-11ee-4094-a7f8-aeeac0411fc3.")
    parser.add_argument("subassessmenttype", type=str, help="The sub assessment type, one of 're3data', 'fuji', or 'selfassessment'.")
    parser.add_argument("subassessmentcategory", type=str, help="The new sub assessment category to set, one of 'official' (to set official status) or 'none' (to remove official status).")

    args = parser.parse_args()
    repo_id = args.repoid
    assessment_id = args.assessmentid
    subassessmentuuid = args.subassessmentuuid
    subassessmenttype = args.subassessmenttype
    subassessmentcategory = args.subassessmentcategory

    if not subassessmentcategory in ["official", "none"]:
        print(f"ERROR: argument subassessmentcategory must be one of 'official' or 'none'.")
        sys.exit(1)

    if not subassessmenttype in ["re3data", "fuji", "selfassessment"]:
        print(f"ERROR: argument subassessmenttype must be one of 're3data', 'fuji', or 'selfassessment'.")
        sys.exit(1)

    set_subassessment_category_cmdline(repo_id, assessment_id, subassessmentuuid, subassessmenttype, subassessmentcategory)



def run_assessment():
    """
    Run sub assessment for a repository, given by its cordra ID.
    This is the main function for the lbl_runassessment command line script.
    """
    parser = argparse.ArgumentParser(description="Run assessment")

    parser.add_argument(
        "asmtype",
        type=str,
        help="Assessment type, one of 're3data' or 'fuji'. Note: To run a selfassessment, combine the command line apps 'lbl_asmtemplate', vim and 'lbl_saveassessment'.",
    )
    parser.add_argument(
        "repo",
        type=str,
        help='The internal cordra identifier of the repo to which to assign the assessment. Repo with that ID must exist in database. Use command line app "lbl_listrepos" to find it.',
    )
    parser.add_argument(
        "--numds",
        type=int,
        default=3,
        help='The number of datasets to sample for a F-UJI assessment. Ignored for other asmtypes.',
    )
    parser.add_argument(
        "--apiurl", type=str, default=None, help='The API URL to use for asmtype "fuji". Ignored for other asmtypes. Set to "auto" to use the one stored in re3data/KH.'
    )
    parser.add_argument(
        "--store",
        action="store_true",
        help="Whether to store the result in Cordra, in addition to printing it to stdout.",
    )
    parser.add_argument(
        "--silent",
        action="store_true",
        help="Set this to suppress output of the assessment result on the command line.",
    )
    args = parser.parse_args()

    asmtype = args.asmtype

    valid_run_asmtypes = ["re3data", "fuji"]
    if asmtype not in valid_run_asmtypes:
        raise ValueError(
            f"ERROR: argument asmtype must be one of {valid_run_asmtypes}. Note: To run a selfassessment, combine the command line apps 'lbl_asmtemplate', vim and 'lbl_saveassessment'."
        )

    repo_id = args.repo
    apiurl = args.apiurl

    repo_name = get_unique_repo_name_for_id(repo_id)
    if repo_name is None:
        raise ValueError(f"fuji assessment failed: could not retrieve repo name for repo with id '{repo_id}'.")

    print(f"Using repo with ID '{repo_id}' with name '{repo_name}'.")

    if asmtype == "fuji":
        if apiurl is None or apiurl == "auto":
            try:
                detected_api_url = get_repo_oai_pmh_api_url(repo_id)
                if detected_api_url is not None:
                    apiurl = detected_api_url
                    print(f"Note: The following auto-detected API URL from cordra will be used for the repo '{repo_name}' with ID '{repo_id}': {detected_api_url}")
                else:
                    print(f"Note: No API listed in re3data/KH for repo {repo_name} with ID '{repo_id}'.")
                    sys.exit(1)

            except Exception as ex:
                pass  # This is just a convenience try. We don't really care if it fails.
                print(f"ERROR: Failed to auto-detect API URL for repo {repo_name} with ID '{repo_id}': {str(ex)}. Please use argument '--apiurl' to specify URL manually.")
                sys.exit(1)

        fuji_eval_result, fuji_status_string, fuji_json_details = compute_repo_fuji_eval_result(repo_url=apiurl, repo_name=repo_name, selection_n=args.numds)
        if args.store:
            save_fuji_assessment_to_cordra(repo_id, fuji_eval_result)
            print(f"Assessment of type '{asmtype}' stored in db for repo {repo_name} with ID {repo_id}.")
        if not args.silent:
            print(f"Assessment type '{asmtype}' result for repo {repo_name} with ID {repo_id}:")
            print(json.dumps(fuji_eval_result, ensure_ascii=True, indent=4))
            print(f"fuji_status_string: {fuji_status_string}")
            print(f"fuji_json_details: ")
            print(json.dumps(fuji_json_details, ensure_ascii=True, indent=4))
    else:  # re3data
        label_eval_result = compute_repo_re3data_eval_result(repo_id, repo_name)
        re3data_eval_result = label_eval_result["results"]["re3data"][0]
        if args.store:
            save_re3data_assessment_to_cordra(repo_id, re3data_eval_result)
            print(f"Assessment of type '{asmtype}' stored in db for repo {repo_name} with ID {repo_id}.")
        if not args.silent:
            print(f"Assessment type '{asmtype}' result for repo {repo_name} with ID {repo_id}:")
            print(json.dumps(re3data_eval_result, ensure_ascii=True, indent=4))


def save_subassessment():
    """
    Save sub assessment to cordra database. Can generate one if needed, or read it from a JSON file.
    This is the main function for the lbl_saveassessment command line script.
    """
    parser = argparse.ArgumentParser(description="Save sub assessment")

    parser.add_argument("asmtype", type=str, help="Assessment type, one of 're3data', 'fuji', or 'selfassessment'.")
    parser.add_argument(
        "--afile",
        type=str,
        default=None,
        help="Optional, path to a JSON file containing the assessment. If omitted, a default assessment will be used. If you supply one yourself, you must make sure it conforms to the schema. You can use the command line tool 'lbl_asmtemplate' to generate one, then edit it in a text editor.",
    )
    parser.add_argument(
        "repoid",
        type=str,
        help="The internal identifier of the repo to which to assign the assessment. Repo with that ID must exist in database. Run the lbl_listrepos app to find repo IDs.",
    )
    parser.add_argument(
        "--achieved",
        action="store_true",
        help="Only valid if --afile and --random are NOT set. If set, the generated assessment has all positive results. Omit this (and --random) to get all negative results.",
    )
    parser.add_argument(
        "--random",
        action="store_true",
        help="Only valid if --afile and --achieved are NOT set. If set, the generated assessment has random results for the metrics. Use --achieved (and omit this) to get positive results. Use neither this nor --achieved to get all negative results.",
    )
    parser.add_argument(
        "--silent", action="store_true", help="Set this to suppress output of the saved assessment on the command line."
    )
    args = parser.parse_args()

    asmtype = args.asmtype
    repo_id = args.repoid

    if not repo_id.startswith("n4e"):
        print("ERROR: repo ID must start with 'n4e'.")
        sys.exit(1)

    if args.random and args.achieved:
        print("ERROR: arguments --random and --achieved are incompatible.")
        sys.exit(1)

    if args.random and args.afile:
        print("ERROR: arguments --random and --afile are incompatible.")
        sys.exit(1)

    valid_asmtypes = ["re3data", "fuji", "selfassessment"]
    if asmtype not in valid_asmtypes:
        raise ValueError(f"ERROR: argument asmtype must be one of {valid_asmtypes}")

    if args.achieved and args.afile:
        print("ERROR: arguments --achieved and --afile are incompatible.")
        sys.exit(1)

    result_value = args.achieved
    result_value_string = "positive" if result_value else "negative"
    if args.random:
        result_value = None # None means random
        result_value_string = "random"

    if not args.silent:
        print(f"Saving sub assessment of type '{asmtype}' to repo with ID '{repo_id}'. Metric results will be {result_value_string}.")

    acontent = None
    if args.afile:
        with open(args.afile) as f:
            acontent = json.load(f)
            if not "metrics" in acontent.keys():
                print("Invalid sub assessment: missing field 'metrics'.")
                sys.exit(1)

    if acontent is None:
        if asmtype == "re3data":
            acontent = json.loads(json.dumps(get_eval_default_re3data_entry(result_value=result_value)))
            if not "assignsPersistentIds" in acontent["metrics"].keys():
                print("Invalid re3data sub assessment: missing field 'metrics.assignsPersistentIds'")
                sys.exit(1)
        elif asmtype == "selfassessment":
            acontent = json.loads(json.dumps(get_eval_default_selfassessment_entry(result_value=result_value)))
            if not "hasCuration" in acontent["metrics"].keys():
                print("Invalid selfassessment sub assessment: missing field 'metrics.hasCuration'")
                sys.exit(1)
        else: # fuji
            acontent = json.loads(json.dumps(get_eval_default_fuji_entry(result_value=result_value)))
            if not "hasDescriptiveMetadata" in acontent["metrics"].keys():
                print("Invalid fuji sub assessment: missing field 'metrics.hasDescriptiveMetadata'")
                sys.exit(1)

    save_assessment_part_to_cordra(repo_id=repo_id, acontent=acontent, asmtype=asmtype)

    if not args.silent:
        print(json.dumps(acontent, ensure_ascii=True, indent=4))

    repo_id_url = repo_id.replace("/", "%2F")
    print(f"Upload done. Check: http://localhost:3000/verify/repository/{repo_id_url}")


def app_status():
    parser = argparse.ArgumentParser(description="Check Label App Status")
    parser.add_argument("--test-email", type=str, help="Send a test email to given address to test email server configuration.")
    parser.add_argument("--silent", action="store_true", help="Flag, whether to print only critical errors and keep silent for the rest.")
    args = parser.parse_args()

    silent = args.silent
    testemail = args.test_email

    check_label_app_status(silent=silent, raise_exception=False)


    if testemail is not None:
        if ENABLE_SMTP:
            if LABEL_SMTP_SECRET_MAIL_USER is None or LABEL_SMTP_SECRET_MAIL_PASSWORD is None:
                print("Cannot send test email: No SMTP credentials provided.")
            else:
                send_testemail(testemail)
                if not silent:
                    print(f"Test email sent to {testemail}.")
        else:
            print("Cannot send test email: SMTP is disabled in settings.")




