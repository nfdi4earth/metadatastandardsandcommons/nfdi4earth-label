# shared data structures and definitions

from enum import Enum
from datetime import datetime
from typing import List, Dict, Union
import dateutil
import uuid
from .webapi import WebApi
from .datamodel import get_subassessment_score_object, get_singlemetric_score_object
from .subasm_scoring import compute_subasm_passed_selfassessment, compute_subasm_passed_re3data, compute_subasm_passed_fuji
import logging
import random
from .subasm_scoring import metrics
from .fuji_eval_status import FujiEvalStatus

logger = logging.getLogger(__name__)

evaluated_by_default_value = "unknown"

class AssessmentPart(Enum):
    """
    This models the 3 entries in the results section of the schema of NFDI4Earth KH objects of type 'RepositoryEvalForLabel'.
    The value strings should match the names in the JSON data structure (see devops/cordra/JsonSchema/RepositoryEvalForLabel.json in backend repo)
    """

    RE3DATA = "re3data"
    SELFASSESSMENT = "selfassessment"
    FUJI = "fuji"


def get_datetime_now_in_cordra_format():
    """Get a date time string of the current date/time in the format configured in cordra.
    This function is the central place where we do this, to prevent having to change the format
    in many places if we adapt the expected format in Cordra.
    """
    dt_string = datetime.now().isoformat()
    dt_string = dt_string[:-3]
    dt_string += "Z"
    return dt_string


def get_latest_eval_instance(eval_array: List[Dict]) -> Union[Dict, None]:
    """
    Given a list of eval instances (e.g., from ```RepositoryEvalForLabel -> results -> re3data```, or ```RepositoryEvalForLabel -> results -> selfassessment```) get the relevant one.
    @return the most recent entry, according to the date in the 'evaluated_at' field.
    """
    num_results = len(eval_array)
    if num_results == 0:
        return None
    else:
        sorted_ar = sorted(eval_array, key=lambda item: parse_cordra_datetime_field_to_date(item["evaluated_at"]))
        return sorted_ar[-1]


def get_official_eval_instance(eval_array: List[Dict], uuid: str) -> Union[Dict, None]:
    """
    Given a list of eval instances (e.g., from ```RepositoryEvalForLabel -> results -> re3data```, or ```RepositoryEvalForLabel -> results -> selfassessment```) get the official one with the given UUID.
    @return the offical entry, according to the requested UUID in the 'uuid' field.
    """
    hits = [item for item in eval_array if item["uuid"] == uuid]
    if len(hits) == 1:
        return hits[0]
    return None


def parse_cordra_datetime_field_to_date(field: str):
    """
    Return a Python date instance from the string contained in Cordra date fields.
    @note See also function `get_datetime_now_in_cordra_format`
    """
    return dateutil.parser.parse(field)


def generate_uuid() -> str:
    """Generate a unique ID. Used for sub assessment IDs."""
    return str(uuid.uuid4())


def get_eval_default(repo_name="", repo_handle=None) -> dict:
    """
    Get the default (empty) evaluation result data structure. One needs to fill this out (by running the evaluation, including re3data and F-UJI) and then store it back to the database.

    :param with_fuji: if True, the F-UJI part is included in the result. If False, only the re3data part is included.
    return: the default evaluation result data structure.
    """
    empty_uuid = ""
    eval_default = {
        "n4e:repository": {
            "name": repo_name,
            "handle": repo_handle,
        },
        "evaluated_at": get_datetime_now_in_cordra_format(),
        "evaluated_by": evaluated_by_default_value,  # will be replaced with user handle later
        "evaluation_version": get_evaluation_version(),
        "results": {
            "re3data": [],
            "fuji": [],
            "selfassessment": [],
            "achievedN4ELabel": {
                "official" : {
                    "result": False,
                    "score_re3data": 0,
                    "score_re3data_max_possible": 0,
                    "score_fuji": 0,
                    "score_fuji_max_possible": 0,
                    "score_selfassessment": 0,
                    "score_selfassessment_max_possible": 0,
                    "passed_re3data": False,
                    "passed_fuji": False,
                    "passed_selfassessment": False,
                    "based_on_subassessments" : {
                        "re3data_subassessment_uuid" : empty_uuid,
                        "fuji_subassessment_uuid" : empty_uuid,
                        "selfassessment_subassessment_uuid" : empty_uuid
                    },
                    "evaluated_at": get_datetime_now_in_cordra_format(),
                    "suggestions" : { "re3data" : [], "fuji" : [], "selfassessment" : [] }
                },
                "latest" : {
                    "result": False,
                    "score_re3data": 0,
                    "score_re3data_max_possible": 0,
                    "score_fuji": 0,
                    "score_fuji_max_possible": 0,
                    "score_selfassessment": 0,
                    "score_selfassessment_max_possible": 0,
                    "passed_re3data": False,
                    "passed_fuji": False,
                    "passed_selfassessment": False,
                    "based_on_subassessments" : {
                        "re3data_subassessment_uuid" : empty_uuid,
                        "fuji_subassessment_uuid" : empty_uuid,
                        "selfassessment_subassessment_uuid" : empty_uuid
                    },
                    "evaluated_at": get_datetime_now_in_cordra_format(),
                    "suggestions" : { "re3data" : [], "fuji" : [], "selfassessment" : [] }
                },
            },
            "evaluated_at": get_datetime_now_in_cordra_format(),
        },
    }

    return eval_default.copy()


def get_evaluation_version() -> int:
    """
    The evaluation version. This should be increased whenever metrics are changed once this software is officially released.
    """
    return 1


def get_score_method(result_value: Union[bool, None]) -> str:
    """
    Get the score method for a given result value.
    """
    if result_value is None:
        return "random"
    return "max" if result_value else "min"


def compute_result(result_value: Union[bool, None]) -> bool:
    """
    Get the result value for a given result value. May include random draw.
    """
    if result_value is None:
        # return a random boolean value
        return bool(random.getrandbits(1))
    return result_value


def get_eval_default_re3data_entry(result_value: Union[bool, None] = False) -> dict:
    """
    Return a template for the re3data assessment.
    @param result_value: The value to use for the results.
    """
    subasmtype = "re3data"
    score_method = get_score_method(result_value)
    logger.debug(f"Generating re3data assessment template with score_method: {score_method}")
    max_singlemetric_score = 1
    part = {
        "uuid": generate_uuid(),
        "evaluated_at": get_datetime_now_in_cordra_format(),
        "evaluated_by": evaluated_by_default_value,  # will be replaced with user handle later
        "evaluation_version" : get_evaluation_version(),
        "subassessmentscore" : None, # Will be computed below
        "metrics" : {
            "assignsPersistentIds": {
                "result": compute_result(result_value),
                "reason": {"supports": list()},
                "basedOn": subasmtype,
                "description_short": "Assigns persistent IDs",
                "singlemetricscore": None # Will be computed below
            },
            "assignsUniqueIds": {
                "result": compute_result(result_value),
                "reason": {"supports": list()},
                "basedOn": subasmtype,
                "description_short": "Assigns unique IDs",
                "singlemetricscore": None # Will be computed below
            },
            "providesAnyAPI": {
                "result": compute_result(result_value),
                "reason": {"supports": list(), "endpoints": list()},
                "basedOn": subasmtype,
                "description_short": "Provides any API",
                "singlemetricscore": None # Will be computed below
            },
            "supportsApisHarvestable": {
                "result": compute_result(result_value),
                "reason": {"supports": list(), "endpoints": list()},
                "basedOn": subasmtype,
                "description_short": "Provides harvestable API",
                "singlemetricscore": None # Will be computed below
            },
            "supportsCrossDomainStandard": {
                "result": compute_result(result_value),
                "reason": {"supports": list()},
                "basedOn": subasmtype,
                "description_short": "Supports cross-domain metadata standards",
                "singlemetricscore": None # Will be computed below
            },
            "supportsSpecificESSStandard": {
                "result": compute_result(result_value),
                "reason": {"supports": list()},
                "basedOn": subasmtype,
                "description_short": "Supports domain-specific metadata standards",
                "singlemetricscore": None # Will be computed below
            }
        }
    }
    # Compute the singlemetricscore for all metrics, based on the metric.result field
    for metric_name in list(part["metrics"].keys()):
        metric = part["metrics"][metric_name]
        achieved_score = max_singlemetric_score if metric["result"] else 0
        score_not_achieved_suggestion = metrics[subasmtype][metric_name]["suggestion"]
        required_for_label = metrics[subasmtype][metric_name]["required_for_label"]
        metric["singlemetricscore"] = get_singlemetric_score_object(max_singlemetric_score, achieved_score, score_not_achieved_suggestion=score_not_achieved_suggestion, required_for_label=required_for_label)

    # Compute the subassessment score from the metrics in the part
    part["subassessmentscore"] = get_subassessment_score_object(
        sum([metric["singlemetricscore"]["score_achieved"] for metric in part["metrics"].values()]),
        sum([metric["singlemetricscore"]["score_max_possible"] for metric in part["metrics"].values()]),
        score_metrics_considered=[metric_name for metric_name in list(part["metrics"].keys())],
        subassessment_passed = False  # This will be computed below
    )
    part["subassessmentscore"]["subassessment_passed"] = compute_subasm_passed_re3data(part)
    assert "subassessment_passed" in part["subassessmentscore"], "subassessment_passed missing in re3data subassessment"
    assert "score_metrics_considered" in part["subassessmentscore"], "score_metrics_considered missing in re3data subassessment"
    print("###################### OKAY re3data subassessment ########################")
    return part

def get_eval_default_fuji_entry(result_value: Union[bool, None] = False) -> dict:
    """
    Get the default evaluation result dict with a F-UJI evaluation result.
    Returns a single instance, which should be put into a list.
    """
    subasmtype = "fuji"
    score_method = get_score_method(result_value)
    logger.debug(f" * Using score method: {score_method}")
    max_singlemetric_score = 1
    fuji_scan_technical_success = True if result_value is None or result_value == True else False
    status_no_api = FujiEvalStatus.descriptions[FujiEvalStatus.NO_EVAL_DONE_NO_API]
    status_success = FujiEvalStatus.descriptions[FujiEvalStatus.SUCCESS]
    fuji_scan_technical_status_message = status_success if fuji_scan_technical_success else status_no_api
    subasm_fuji = {
        "uuid": generate_uuid(),
        "evaluated_at" : get_datetime_now_in_cordra_format(),
        "evaluated_by" : evaluated_by_default_value,
        "evaluation_version" : get_evaluation_version(),
        "subassessmentscore" : None,  # Computed below
        "scan": {
            "fuji_scan_technical_success": fuji_scan_technical_success,
            "fuji_scan_technical_status_message": fuji_scan_technical_status_message,
            "fuji_scan_datetime": get_datetime_now_in_cordra_format()
        },
        "metrics": {
            "hasDescriptiveMetadata": {
                "result": compute_result(result_value),
                "reason": {
                    "fuji_score": 0.0,
                    "fuji_score_field_name": "F2",
                },
                "description_short": "Dataset has descriptive metadata",
                "basedOn": subasmtype,
                "singlemetricscore": None # Computed below from the result field
            },
            "metadataIncludesIdentifier": {
                "result": compute_result(result_value),
                "reason": {
                    "fuji_score": 0.0,
                    "fuji_score_field_name": "F3",
                },
                "description_short": "Metadata includes dataset identifier",
                "basedOn": subasmtype,
                "singlemetricscore": None # Computed below
            },
            "metadataLinks": {
                "result": compute_result(result_value),
                "reason": {
                    "fuji_score": 0.0,
                    "fuji_score_field_name": "I3",
                },
                "description_short": "Metadata includes links between the data and its related entities",
                "basedOn": subasmtype,
                "singlemetricscore": None # Computed below
            },
            "includesLicense": {
                "result": compute_result(result_value),
                "reason": {
                    "fuji_score": 0.0,
                    "fuji_score_field_name": "R1.1",
                },
                "description_short": "Metadata includes license information",
                "basedOn": subasmtype,
                "singlemetricscore": None # Computed below
            },
            "includesProvenance": {
                "result": compute_result(result_value),
                "reason": {
                    "fuji_score": 0.0,
                    "fuji_score_field_name": "R1.2",
                },
                "description_short": "Metadata includes provenance information",
                "basedOn": subasmtype,
                "singlemetricscore": None # Computed below
            },
            "metadataRepresentation": {
                "result": compute_result(result_value),
                "reason": {
                    "fuji_score": 0.0,
                    "fuji_score_field_name": "FsF-I1-01M",
                },
                "description_short": "Metadata is represented using a formal knowledge representation language",
                "basedOn": subasmtype,
                "singlemetricscore": None # Computed below
            },
            "specifiesContent": {
                "result": compute_result(result_value),
                "reason": {
                    "fuji_score": 0.0,
                    "fuji_score_field_name": "FsF-R1-01MD",
                },
                "description_short": "Metadata specifies the content of the data",
                "basedOn": subasmtype,
                "singlemetricscore": None # Computed below
            }
        }
    }
    # Compute the singlemetricscore for all metrics, based on the metric.result field
    for metric_name in list(subasm_fuji["metrics"].keys()):
        metric = subasm_fuji["metrics"][metric_name]
        achieved_score = max_singlemetric_score if metric["result"] else 0
        score_not_achieved_suggestion = metrics[subasmtype][metric_name]["suggestion"]
        required_for_label = metrics[subasmtype][metric_name]["required_for_label"]
        metric["singlemetricscore"] = get_singlemetric_score_object(max_singlemetric_score, achieved_score, score_not_achieved_suggestion=score_not_achieved_suggestion, required_for_label=required_for_label)
    # Compute the subassessment score from the metrics in the part
    subasm_fuji["subassessmentscore"] = get_subassessment_score_object(
        sum([metric["singlemetricscore"]["score_achieved"] for metric in subasm_fuji["metrics"].values()]),
        sum([metric["singlemetricscore"]["score_max_possible"] for metric in subasm_fuji["metrics"].values()]),
        score_metrics_considered=[metric_name for metric_name in list(subasm_fuji["metrics"].keys())],
        subassessment_passed = False  # Will be computed below
    )
    subasm_fuji["subassessmentscore"]["subassessment_passed"] = compute_subasm_passed_fuji(subasm_fuji)
    return subasm_fuji


def get_eval_default_selfassessment_entry(result_value: Union[bool, None] = False) -> dict:
    """
    Return a template for the selfassessment form-based assessment.
    @param result_value: The value to use for the results.
    """
    subasmtype = "selfassessment"
    score_method = get_score_method(result_value)
    logger.debug(f"Generating re3data assessment template with score_method: {score_method}")
    max_singlemetric_score = 1
    subasm_selfassessment = {
        "uuid": generate_uuid(),
        "evaluated_at": get_datetime_now_in_cordra_format(),
        "evaluated_by": evaluated_by_default_value,  # will be replaced with user handle later
        "evaluation_version": get_evaluation_version(),
        "subassessmentscore" : None, # Will be computed below
        "metrics": {
            "hasBackupStrategy": {
                "result": compute_result(result_value),
                "reason": {"description": None},
                "basedOn": subasmtype,
                "description_short": "Whether the repository has a backup strategy",
                "singlemetricscore": None # Will be computed below
            },
            "hasArchivingStrategy": {
                "result": compute_result(result_value),
                "reason": {"description": None},
                "basedOn": subasmtype,
                "description_short": "Whether the repository has an archiving strategy",
                "singlemetricscore": None # Will be computed below
            },
            "hasCuration": {
                "result": compute_result(result_value),
                "reason": {
                    "description": None,
                    "curation_metadata": result_value,
                    "curation_fileformats": result_value,
                    "curation_data": result_value,
                },
                "basedOn": subasmtype,
                "description_short": "Whether the repository performs some sort of curation as part of the data submission process.",
                "singlemetricscore": None # Will be computed below
            },
            "hasUserSupport": {
                "result": compute_result(result_value),
                "reason": {
                    "description": None,
                    "helpdesk_has": result_value,
                    "helpdesk_url": None,
                    "userinteraction_has": result_value,
                    "userinteraction_url": None,
                },
                "basedOn": subasmtype,
                "description_short": "Whether the repository has some sort of user support.",
                "singlemetricscore": None # Will be computed below
            },
            "hasFundingStatement": {
                "result": compute_result(result_value),
                "reason": {"description": None, "url": None},
                "basedOn": subasmtype,
                "description_short": "Whether the repository has a funding statement.",
                "singlemetricscore": None # Will be computed below
            },
            "hasCertifications": {
                "result": compute_result(result_value),
                "reason": {"name": None, "url": None, "description": None},
                "basedOn": subasmtype,
                "description_short": "Whether the repository has certifications other than CoreTrustSeal. Optional, does not affect the NFDI4Earth Label score.",
                "singlemetricscore": get_singlemetric_score_object(0, 0)   # No score for this metric
            },
        }
    }
    # Compute the singlemetricscore for all metrics, based on the metric.result field
    for metric_name in list(subasm_selfassessment["metrics"].keys()):
        if metric_name == "hasCertifications":
            continue  # No score for this metric
        metric = subasm_selfassessment["metrics"][metric_name]
        achieved_score = max_singlemetric_score if metric["result"] else 0
        score_not_achieved_suggestion = metrics[subasmtype][metric_name]["suggestion"]
        required_for_label = metrics[subasmtype][metric_name]["required_for_label"]
        metric["singlemetricscore"] = get_singlemetric_score_object(max_singlemetric_score, achieved_score, score_not_achieved_suggestion=score_not_achieved_suggestion, required_for_label=required_for_label)
    # Compute the subassessment score from the metrics in the part
    subasm_selfassessment["subassessmentscore"] = get_subassessment_score_object(
        sum([metric["singlemetricscore"]["score_achieved"] for metric in subasm_selfassessment["metrics"].values()]),
        sum([metric["singlemetricscore"]["score_max_possible"] for metric in subasm_selfassessment["metrics"].values()]),
        score_metrics_considered=[metric_name for metric_name in list(subasm_selfassessment["metrics"].keys()) if metric_name != "hasCertifications"],
        subassessment_passed = False  # This will be computed below
    )
    subasm_selfassessment["subassessmentscore"]["subassessment_passed"] = compute_subasm_passed_selfassessment(subasm_selfassessment)
    return subasm_selfassessment


def verify_could_be_subassessment(subassessment: dict, subasmtype : str) -> bool:
    """
    Performs a basic validation that a dict is a valid re3data subassessment entry.
    This only checks that the top-level keys are present.
    """
    valid_subasmtypes = ["re3data", "selfassessment", "fuji"]
    if subasmtype not in valid_subasmtypes:
        raise ValueError(f"verify_could_be_subassessment: Invalid subassessment type: {subasmtype}")

    if subasmtype == "re3data":
        template = get_eval_default_re3data_entry()
    elif subasmtype == "selfassessment":
        template = get_eval_default_selfassessment_entry()
    else: # has to be fuji, checked above.
        template = get_eval_default_fuji_entry()

    missing_toplevel_keys = []

    for key in template.keys():
        if key not in subassessment.keys():
            missing_toplevel_keys.append(key)

    if len(missing_toplevel_keys) > 0:
        logger.error(f"{subasmtype} subassessment is missing the following {len(missing_toplevel_keys)} top-level keys: {missing_toplevel_keys}")
        return False

    return True



def get_repo_api_url(repo_eval_result: dict, api: WebApi) -> Union[str, None]:
    """
    Extract the requested API URL for a repository, if it exists in the eval result. Returns None if no such API URL was found.
    @param repo_eval_result: The full, outer RepositoryEvalForLabel evaluation result for the repository, that must contain at least one re3data sub assessment. The relevant one is extracted and used.
    @return the API URL as a string, or None if none could be found
    """
    apis = get_api_endpoints(repo_eval_result, limit_to_api_types=[api.value])
    return apis.get(api.value, None)


def get_api_endpoints(repo_eval_result: dict, limit_to_api_types=None) -> dict:
    """
    Extract the APIs (API type and endpoint URL) from the eval result, if any. Returns an empty dictionary if no APIs were found.
    These are the APIs that are listed in the re3data.org entry, and this list includes APIs that are not necessarily harvestable.
    This function does not check whether the APIs are actually up, it just returns the list of APIs that are listed in re3data.

    @param repo_eval_result The full, outer RepositoryEvalForLabel evaluation result for the repository, that must contain at least one re3data sub assessment. The relevant one is extracted and used.    @param repo_eval_result: The full, outer RepositoryEvalForLabel evaluation result for the repository, that must contain at least one re3data sub assessment. The relevant one is extracted and used.
    @param limit_to_api_types: If not None, only return endpoints for the given API types (list of strings). Otherwise, return all endpoints. The available
        API types are listed in the ```WebApi``` class, currently in file ```re3data.py```.
    @return mapping of API types to end points, e.g., {"OAI-PMH": "https://www.pangaea.de/oai/provider"}
    """
    apitypes_to_endpoints = {}

    relevant_re3data_eval_instance = get_latest_eval_instance(repo_eval_result["results"]["re3data"])
    if relevant_re3data_eval_instance is None:
        return apitypes_to_endpoints
    else:
        if relevant_re3data_eval_instance["metrics"]["providesAnyAPI"]["result"] is True:
            for idx, api_type in enumerate(relevant_re3data_eval_instance["metrics"]["providesAnyAPI"]["reason"]["supports"]):
                if limit_to_api_types is None or api_type in limit_to_api_types:
                    apitypes_to_endpoints[api_type] = relevant_re3data_eval_instance["metrics"]["providesAnyAPI"]["reason"][
                        "endpoints"
                    ][idx]
    return apitypes_to_endpoints
