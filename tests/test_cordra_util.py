# Copyright 2023, 2024 NFDI4Earth
#
# SPDX-License-Identifier: Apache-2.0

import pytest
from nfdi4earth_label.cordra_util import get_repo_id_for_name



@pytest.mark.remoteservices
@pytest.mark.cordra
def test_get_repo_id_for_name_with_name_in_testdata():
    repo_name = "PANGAEA"
    repo_id = get_repo_id_for_name(repo_name)
    assert repo_id.startswith("n4e"), f"Expected repo_id to start with 'n4e', got {repo_id}"


@pytest.mark.remoteservices
@pytest.mark.cordra
def test_get_repo_id_for_name_with_name_not_in_testdata():
    repo_name = "this name does not exist"
    repo_id = get_repo_id_for_name(repo_name)
    assert repo_id is None, f"Expected repo_id to be None, got {repo_id}"