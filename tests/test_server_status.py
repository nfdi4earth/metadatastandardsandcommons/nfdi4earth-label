# Copyright 2023, 2024 NFDI4Earth
#
# SPDX-License-Identifier: Apache-2.0

from nfdi4earth_label.server_status import ServerStatus, is_http_or_https_url_maybe, is_ftp_url_maybe

def test_server_status():
    status = ServerStatus.SERVER_UP_OK
    assert status.value == "UP", f"Expected status.value to be 'UP', got {status.value}"


def test_is_http_or_https_url_maybe():
    assert is_http_or_https_url_maybe("http://www.example.com") == True, f"Expected is_http_or_https_url_maybe('http://www.example.com') to be True, got {test_is_http_or_https_url_maybe('http://www.example.com')}"
    assert is_http_or_https_url_maybe("bro") == False, f"Expected is_http_or_https_url_maybe('bro') to be True, got {test_is_http_or_https_url_maybe('bro')}"

def test_is_ftp_url_maybe():
    assert is_ftp_url_maybe("ftp://www.example.com") == True, f"Expected is_ftp_url_maybe('ftp://www.example.com') to be True, got {test_is_ftp_url_maybe('ftp://www.example.com')}"
    assert is_ftp_url_maybe("bro") == False, f"Expected is_ftp_url_maybe('bro') to be False, got {test_is_ftp_url_maybe('bro')}"

